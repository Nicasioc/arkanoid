/*
	Adds walls that contain the game area.

	In this example we create simple rectangles.
	
*/

M.registerEntity("walls", function () {
	
	var walls = new M.Entity();

	walls.shows("leftWall").as("rectangle").set({
		width: 8,
		height: M.game.config.CANVAS_HEIGHT - 24,
		left: 0,
		top: M.game.config.CEILING_START,
		color: "#adadad"
	});
	walls.shows("rightWall").as("rectangle").set({
		width: 8,
		height: M.game.config.CANVAS_HEIGHT - 24,
		left: 216,
		top: M.game.config.CEILING_START,
		color: "rgb(200, 200, 200)"
	});
	walls.shows("ceiling").as("rectangle").set({
		width: M.game.config.CANVAS_WIDTH,
		height: 8,
		left: 0,
		top: M.game.config.CEILING_START - 8,
		color: "rgb(190, 190, 190)"
	});
	walls.name = "Walls";

	return walls;

});