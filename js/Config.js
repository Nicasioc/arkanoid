/*
	Basic configuration for the game
*/

M.game.config = {
	CANVAS_WIDTH: 224,
	CANVAS_HEIGHT: 256,
	BALL_SIZE: 5,
	CEILING_START: 24,
	rowColors: ["gray", "red", "yellow", "blue", "purple", "green"],
	startTop: 80,
	startLeft: 8,
	keyboardMappings: {
			left: "left",
			right: "right",
			action: "space"
	},

}
M.game.config.areaToStayIn = {
	top: 24, right: 216, left: 8, bottom: M.game.config.CANVAS_HEIGHT + M.game.config.BALL_SIZE
}