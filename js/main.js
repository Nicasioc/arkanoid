/*
 * This is the entrance point of your game.
 * When the document is finished loading the following function will be executed.
 */
function main() {

	M.sprites.load(["assets/sprites/spritesheet.json"]);

	var CANVAS_WIDTH = M.game.config.CANVAS_WIDTH,
		CANVAS_HEIGHT = M.game.config.CANVAS_HEIGHT,
		BALL_SIZE = M.game.config.BALL_SIZE,
		CEILING_START = M.game.config.CEILING_START,
		ball = M.createEntity("ball"),
		pad = new M.Entity(),
		rowColors = M.game.config.rowColors,
		startTop = M.game.config.startTop,
		startLeft = M.game.config.startLeft,
		areaToStayIn = M.game.config.areaToStayIn,
		keyboardMappings = M.game.config.keyboardMappings,
		block,
		walls = M.createEntity("walls"),
		lives = 2;

	addBlocks();
	//----------------------
	//PAD
	//

	pad.has("speed", 1.25);
	pad.has("areaToStayIn", areaToStayIn);
	pad.TYPE = "pad";

	pad.shows("body").as("rectangle").set({
		x: CANVAS_WIDTH / 2, y: CANVAS_HEIGHT - 16, width: 36, height: 12, color: "white"
	});

	pad.does("move", function () {

		var body = pad.view("body"),
			speed = pad.attribute("speed"),
			move = pad.attribute("move");

		if ( move == 1 ) {
			body.offsetX(-speed);
		} else if ( move == 2 ) {
			body.offsetX(speed);
		}
		
	});
	
	pad.does("stayInArea");
	
	pad.name = "Pad";

	M.registerBehaviour("releaseBall", function () {
		ball.doesnt("stickToPad");
		pad.doesnt("releaseBall");
	});


	M.registerBehaviour("stickToPad", function(entity, attributes, views) {
		padBody = pad.getView("body");
		attributes.get("location").set(padBody.getX(), padBody.getTop() - 2.6);
	});
	ball.does("stickToPad");
	
	ball.name = "Ball";

	M.add(pad).to("world");
	M.add(ball).to("world");
	M.add(walls).to("world");


	//----------------------
	//GAME LOGIC
	//

	pad.addEventListener("keyDown", function (keys) {
		
		if (keys[keyboardMappings.left]) {
			pad.attributes.set("move", 1);
		} else if (keys[keyboardMappings.right]) {
			pad.attributes.set("move", 2);
		} else {
			pad.attributes.set("move", 0);
		}

		if ( keys[keyboardMappings.action] ) {
			pad.does("releaseBall");
		}

	});


	//ball.addEventListener("onCollision", ballCollisionHandler);

	ball.addEventListener("mouseDown", function () {
		console.debug("Hey! don't click the ball!");
	});

	var areaTrigger = new M.AreaTrigger(-50, CANVAS_HEIGHT + 20, CANVAS_WIDTH + 50, 50);

	areaTrigger.onObjectInArea(ball, function(entity, trigger) {
		trigger.disable();
		pad.doesnt("move");
		ball.doesnt("moveWithSpeedAndDirection");
		alert("GAME OVER");
	});

	M.add(areaTrigger);


	//THE FOLLOWING CODE IS EXPLANATORY ONLY
	pad.addEventListener("onCollision", function (manifold) {


		switch ( manifold.otherEntity.TYPE ) {

			case "PowerUp:Shooting":

				var timer = new M.TimeTrigger();
				timer.setInterval(100);
				timer.addEffect(function () {
					//CREATE BULLET CENTERED AT PAD
				});

				M.add(timer);

				break;

			case "PowerUp:Stick":
				ball.does("stickToPad");
				break;

			case "PowerUp:AddTwoBalls":

				for ( var i = 0; i < 2; i++ ) {

					var newBall = M.createEntity("ball");

					newBall.location.set(ball.attribute("location").x, ball.attribute("location").y);

					newBall.addEventListener("onCollision", function (manifold) {

				 		var direction = manifold.entity.attributes.get("direction"),
							location = manifold.entity.attributes.get("location");

						if ( manifold.collisionInX ) {
							direction.set(-direction.x, direction.y);
						} else if ( manifold.collisionInY ) {
							direction.set(direction.x, -direction.y);
						}

						if ( manifold.collidedWith.TYPE == 1 ) {
							M.remove(manifold.collidedWith).from("world");
						}

					});

					newBall.addEventListener("mouseDown", function () {
						console.debug("Hey! don't click the ball!");
					});

					M.add(newBall).to("world");

				}

				break;

			case "PowerUp:TurnToFireBalls":
				break;

		}


	});
}

function addBlocks() {
	var row, column;
	for (row = 0; row < 6; row++) {
		for (column = 0; column < 13; column++) {
			
			block = new M.Entity();

			block.shows("block").as("rectangle").set({
				width: 16,
				height: 8,
				x: M.game.config.startLeft + column * 16 + 8,
				y: M.game.config.startTop + row * 8 + 4,
				color: M.game.config.rowColors[row],
				strokeStyle: null
			});

			block.TYPE = "block";
			
			block.name = "block";

			M.add(block).to("blocks");

		}
	}
}