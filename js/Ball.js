/*
	BALL
*/

//First we register ententy ball
M.registerEntity("ball", function () {
	var ball = new M.Entity();

	// We provide the ball with some atributes
	ball.has("preventMoveOnCollision", true);
	ball.has("areaToStayIn", M.game.config.areaToStayIn);
	ball.has("direction").set(1,-1);
	ball.has("speed", 1);
	ball.has("location").set(M.game.config.CANVAS_WIDTH / 2, M.game.config.CANVAS_HEIGHT - 25);
	ball.has("rotation", 0);

	//as entities dont have any visual representation we tell it to br a small rectangle.
	ball.shows("body").as("rectangle").set({
		width: 5, height: 5, color: "red", x: 0, y: 0
	});

	//now we tell it how to behave
	ball.does("collide");
	ball.does("moveWithSpeedAndDirection");
	ball.does("fixViewsToEntity");

	//As we told the ball that it can collide we can add a listener to onCollision
	//you can find "ballCollisionHandler" on logic.js
	ball.addEventListener("onCollision", ballCollisionHandler);

	return ball;
});